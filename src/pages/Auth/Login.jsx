import { useState } from "react";
import { useNavigate } from "react-router-dom";
import logo from "../../assets/logo.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBookmark } from "@fortawesome/free-solid-svg-icons";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [showAlert, setShowAlert] = useState(false);
  const navigate = useNavigate();

  const handleUsernameChange = (e) => {
    setUsername(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    // Lakukan validasi disini, misalnya dengan mengirim data login ke server
    if (username === "admin" && password === "admin") {
      // Login berhasil
      setShowAlert(false);
      navigate("/dashboard");
      console.log("Login berhasil");
    } else {
      // Login gagal
      setShowAlert(true);
    }
  };

  return (
    <div
      className="container-fluid d-flex align-items-center justify-content-center vh-100 "
      style={{ backgroundColor: "#134897" }}
    >
      <div className="row col-sm-12 col-md-6 col-lg-3 justify-content-center text-light">
        {showAlert && (
          <div
            className="alert alert-danger alert-dismissible fade show"
            role="alert"
          >
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="alert"
              aria-label="Close"
              onClick={() => setShowAlert(false)}
            ></button>
            <strong>Login failed.</strong> Please check your email and password.
          </div>
        )}
        <div className="d-flex justify-content-center">
          <img src={logo} className="w-100" />
        </div>
        <div className="card-body">
          <form onSubmit={handleSubmit}>
            <div className="row g-2 align-items-center mb-3">
              <div className="col-3">
                <label htmlFor="Username" className="col-form-label">
                  Username
                </label>
              </div>
              <div className="col-9">
                <input
                  type="password"
                  id="username"
                  className="form-control"
                  placeholder="Username"
                  value={username}
                  onChange={handleUsernameChange}
                />
              </div>
            </div>
            <div className="row g-2 align-items-center mb-4">
              <div className="col-3">
                <label htmlFor="password" className="col-form-label">
                  Password
                </label>
              </div>
              <div className="col-9">
                <input
                  type="password"
                  id="password"
                  className="form-control"
                  placeholder="password"
                  value={password}
                  onChange={handlePasswordChange}
                />
              </div>
            </div>
            <div className="d-grid">
              <button type="submit" className="btn btn-danger">
                <FontAwesomeIcon
                  icon={faBookmark}
                  style={{ paddingRight: "10px" }}
                />
                Login
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Login;
