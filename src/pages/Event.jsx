import React from "react";
import SidebarComponent from "../components/SidebarComponent";
import NavbarComponent from "../components/NavbarComponent";
import EventComponent from "../components/EventComponent";

function Dashboard() {
  return (
    <div className="container-fluid d-flex flex-row">
      <div className="p-2" style={{ width: '25%' }}>
        <SidebarComponent />
      </div>
      <div className="p-2 flex-grow-1">
        <NavbarComponent />
        <EventComponent/>
      </div>
    </div>
  );
}

export default Dashboard;
