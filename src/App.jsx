// import { useState } from "react";
import Login from "./pages/Auth/Login";
import Category from "./pages/Category";
import Dashboard from "./pages/Dashboard";
import FileManager from "./pages/FileManager";
import Event from "./pages/Event";
import Profile from "./pages/Profile";
// import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { Route, Routes } from "react-router-dom";
// const queryClient = new QueryClient({
//   defaultOptions: {
//     queries: {
//       refetchOnWindowFocus: false,
//     },
//   },
// });

function App() {
  // const [token, setToken] = useState(true);
  // console.log(token);
  // if (token) {
  //   return <Login setToken={setToken} />;
  // }
  // return (
  //   <QueryClientProvider client={queryClient}>
  //     <Dashboard />
  //   </QueryClientProvider>
  // );
  return (
    <Routes>
      <Route path="/" element={<Login />} />
      <Route path="/dashboard" element={<Dashboard />} />
      <Route path="/Category" element={<Category />} />
      <Route path="/Event" element={<Event />} />
      <Route path="/FileManager" element={<FileManager />} />
      <Route path="/Profile" element={<Profile />} />
      
    </Routes>
  );
}

export default App;
