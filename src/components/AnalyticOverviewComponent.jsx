import React from 'react';

const AnalyticOverview = () => {
  return (
    <div className="card mt-2">
      <div className="card-body">
        <h5 className="card-title">Analytic Overview</h5>
        <hr className="hr" />
        <div className="row">
          <div className="col align-self-start" style={{ marginTop: '19px' }}>
            <div className="card" style={{ width: '20rem', backgroundColor: 'rgb(0, 208, 255)' }}>
              <div className="card-body">
                <h1 className="card-title" style={{ textAlign: 'center' }}>250k</h1>
                <p></p>
                <h4 style={{ textAlign: 'center', textDecorationColor: 'aliceblue' }}>Website Visits</h4>
                <h8 style={{ textAlign: 'center', fontSize: '12px', marginLeft: '20%', textDecorationColor: 'darkgray' }}>15% Increase From Last Week</h8>
              </div>
            </div>
          </div>
          <div className="col align-self-start" style={{ marginTop: '19px' }}>
            <div className="card" style={{ width: '20rem', backgroundColor: 'rgb(255, 136, 243)' }}>
              <div className="card-body">
                <h1 className="card-title" style={{ textAlign: 'center' }}>250k</h1>
                <p></p>
                <h4 style={{ textAlign: 'center' }}>Website Visits</h4>
                <h8 style={{ textAlign: 'center', fontSize: '12px', marginLeft: '20%', marginBottom: '30%' }}>15% Increase From Last Week</h8>
              </div>
            </div>
          </div>
          <div className="col align-self-start" style={{ marginTop: '19px' }}>
            <div className="card" style={{ width: '20rem', backgroundColor: 'rgb(255, 136, 243)' }}>
              <div className="card-body">
                <h1 className="card-title" style={{ textAlign: 'center' }}>250k</h1>
                <p></p>
                <h4 style={{ textAlign: 'center' }}>Website Visits</h4>
                <h8 style={{ textAlign: 'center', fontSize: '12px', marginLeft: '20%', marginBottom: '30%' }}>15% Increase From Last Week</h8>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AnalyticOverview;
