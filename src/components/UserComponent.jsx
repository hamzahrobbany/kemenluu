import React from 'react'

const UserComponent = () => {
  return (
    <div className="container-fluid">
    <div className="card" style={{ marginTop: '20px', width: '70%', marginLeft: '0px' }}>
      <div className="card-body">
        <div className="card-title">
          <h5>Profil</h5>
        </div>
        <hr className="hr" />
        <div className="row justify-content-start">
          <div className="col-4">
            <img
              style={{ height: '267px' }}
              src="https://mdbcdn.b-cdn.net/img/new/standard/city/042.webp"
              className="img-fluid shadow-2-strong"
              alt="Palm Springs Road"
            />
            <label className="form-label" htmlFor="customFile">
              Upload Gambar
            </label>
            <input type="file" className="form-control" id="customFile" />
          </div>
          <div className="col-6">
            <div className="form-outline mb-4">
              <input type="input" id="form1Example1" className="form-control" />
              <label className="form-label" htmlFor="form1Example1">
                Full Name
              </label>
            </div>
            <div className="form-outline mb-4">
              <input type="email" id="form1Example2" className="form-control" />
              <label className="form-label" htmlFor="form1Example2">
                Email Address
              </label>
            </div>
            <div className="form-outline mb-4">
              <input type="number" id="form1Example3" className="form-control" />
              <label className="form-label" htmlFor="form1Example3">
                Telephone Number
              </label>
            </div>
            <div className="form-outline mb-4">
              <input type="email" id="form1Example4" className="form-control" />
              <label className="form-label" htmlFor="form1Example4">
                Username
              </label>
            </div>
            <div className="form-outline mb-4">
              <input type="password" id="form1Example5" className="form-control" />
              <label className="form-label" htmlFor="form1Example5">
                Password
              </label>
            </div>
            <input name="" id="" className="btn btn-primary" type="button" value="Save" />
          </div>
        </div>
      </div>
    </div>
  </div>
  )
}

export default UserComponent