import React from 'react';

const EventComponent = () => {
  return (
    <div className="container-fluid">
      <div className="card" style={{ marginTop: '20px', width: '70%', marginLeft: '0px' }}>
        <div className="card-body">
          <div className="card-title">
            <h5>Profil</h5>
          </div>
          <hr className="hr" />
          <div className="row justify-content-start">
            <div className="col-6">
              <div className="form-outline mb-4">
                <input type="input" id="form1Example1" className="form-control" />
                <label className="form-label" htmlFor="form1Example1">Nama Event</label>
              </div>
              <div className="form-outline mb-4">
                <input type="email" id="form1Example1" className="form-control" />
                <label className="form-label" htmlFor="form1Example1">Date</label>
              </div>
              <div className="form-outline mb-4">
                <input type="number" id="form1Example1" className="form-control" />
                <label className="form-label" htmlFor="form1Example1">Location</label>
              </div>
              <form>
                <div className="form-outline mb-4"></div>
                <div className="form-outline mb-4">
                  <input type="password" id="form1Example2" className="form-control" />
                  <label className="form-label" htmlFor="form1Example2">Password</label>
                </div>
                <input name="" id="" className="btn btn-primary align-content-end" type="button" value="Save" />
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EventComponent;
