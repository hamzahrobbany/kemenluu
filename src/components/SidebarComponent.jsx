import React from "react";
import { CDBSidebar, CDBSidebarMenuItem, CDBSidebarContent, CDBSidebarMenu } from "cdbreact";
import { Link } from "react-router-dom";

const SidebarComponent = () => {
  return (
    <CDBSidebar textColor="#333" backgroundColor="#f0f0f0" style={{ height: "100vh" }} className="rounded">
      <div style={{ backgroundColor: "#134897", display: "flex", alignItems: "center", justifyContent: "center", padding: "10px",   }}>
        <img src="src/assets/logo.png" alt="Logo" style={{  height: "150px" }} />
      </div>

      <CDBSidebarContent>
        <CDBSidebarMenu>
        <Link to="/Dashboard">
          <CDBSidebarMenuItem icon="dashboard">Dashboard</CDBSidebarMenuItem>
          </Link>
          <Link to="/event">
          <CDBSidebarMenuItem icon="list-note">Event</CDBSidebarMenuItem>
          </Link>
          <Link to="/Category">
          <CDBSidebarMenuItem icon="list-note">Category</CDBSidebarMenuItem>
          </Link>
          <Link to="/FileManager">
          <CDBSidebarMenuItem icon="folder" iconType="solid">File Manager</CDBSidebarMenuItem>
          </Link>
          <Link to="/Profile">
          <CDBSidebarMenuItem icon="people" iconType="solid">User</CDBSidebarMenuItem>
          </Link>
        </CDBSidebarMenu>
      </CDBSidebarContent>

      <div style={{ backgroundColor: "#134897", padding: "20px", textAlign: "center" }}>
        <button className="btn btn-light">Logout</button>
      </div>
    </CDBSidebar>
  );
};

export default SidebarComponent;
