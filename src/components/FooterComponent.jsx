import React from "react";

const FooterComponent = () => {
  const footerStyle = {
    backgroundColor: "#134897",
    color: "#fff",
    padding: "50px",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  };

  const logoStyle = {
    marginRight: "0px",
    width: "150px",
    height: "100px",
  };

  const textContainerStyle = {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    textAlign: "center",
  };

  const socialIconStyle = {
    marginRight: "0px",
    width: "50px",
    height: "50px",
  };

  return (
    <footer style={footerStyle}>
      <div className="footer-left">
        <img src="src/assets/logo.png" alt="Logo" style={logoStyle} />
      </div>
      <div className="footer-center">
        <div style={textContainerStyle}>
          <p>KEMENTERIAN LUAR NEGERI REPUBLIK INDONESIA</p>
          <p>Jl. Taman Pejambon No. 6 Jakarta Pusat Telp. (62-21) 3849413, 3456014, 3441508 Fax. (62-21) 3855481</p>
        </div>
      </div>
      <div className="footer-right">
        <a href="https://www.instagram.com" target="_blank" rel="noopener noreferrer">
          <img src="src/assets/instagram.png" alt="Instagram" style={socialIconStyle} />
        </a>
        <a href="https://www.twitter.com" target="_blank" rel="noopener noreferrer">
          <img src="src/assets/twitter.png" alt="Twitter" style={socialIconStyle} />
        </a>
        <a href="https://www.youtube.com" target="_blank" rel="noopener noreferrer">
          <img src="src/assets/youtube.png" alt="YouTube" style={socialIconStyle} />
        </a>
        <a href="https://www.facebook.com" target="_blank" rel="noopener noreferrer">
          <img src="src/assets/facebook.png" alt="Website" style={socialIconStyle} />
        </a>
      </div>
    </footer>
  );
};

export default FooterComponent;
