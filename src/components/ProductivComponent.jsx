import React from 'react';

const ProductivComponent = () => {
  return (
    <div className="col align-self-start" style={{ marginTop: '10px' }}>
      <div className="card">
        <img className="card-img-top" src="images.jpg" alt="Title" />

        <div className="card-body">
          <h4 className="text-center">Adam Mackinnon</h4>
          <h6 className="text-center">@username</h6>
          <div className="card">
            <div className="card-body">
              <i className="far fa-image" style={{ marginRight: '10px' }}></i>
              <a style={{ marginRight: '200px' }}>Total Posting Photos</a>
              <a>25.6k</a>
            </div>
          </div>
          <div className="card mt-2">
            <div className="card-body">
              <i className="far fa-image" style={{ marginRight: '10px' }}></i>
              <a style={{ marginRight: '181px' }}>Total Photos Download</a>
              <a>25.6k</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductivComponent;
