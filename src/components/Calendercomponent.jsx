import React from "react";

const Calendercomponent = () => {
  const renderCalendar = () => {
    const today = new Date();
    const currentYear = today.getFullYear();
    const currentMonth = today.getMonth();
    
    // Get the first day of the current month
    const firstDay = new Date(currentYear, currentMonth, 1);
    const startingDay = firstDay.getDay();
  
    // Get the number of days in the current month
    const lastDay = new Date(currentYear, currentMonth + 1, 0);
    const numDays = lastDay.getDate();
  
    // Create an array to store the calendar days
    const calendarDays = [];
  
    // Fill the array with empty slots for the days before the first day of the month
    for (let i = 0; i < startingDay; i++) {
      calendarDays.push(<td key={`empty-${i}`}></td>);
    }
  
    // Fill the array with the days of the month
    for (let day = 1; day <= numDays; day++) {
      calendarDays.push(<td key={day}>{day}</td>);
    }
  
    // Split the calendar days into weeks
    const weeks = [];
    let week = [];
  
    calendarDays.forEach((day, index) => {
      week.push(day);
  
      if ((index + 1) % 7 === 0) {
        weeks.push(<tr key={weeks.length}>{week}</tr>);
        week = [];
      }
    });
  
    // Add the remaining days to the last week
    if (week.length > 0) {
      weeks.push(<tr key={weeks.length}>{week}</tr>);
    }
  
    // Render the calendar
    return (
      <table>
        <thead>
          <tr>
            <th>Sun</th>
            <th>Mon</th>
            <th>Tue</th>
            <th>Wed</th>
            <th>Thu</th>
            <th>Fri</th>
            <th>Sat</th>
          </tr>
        </thead>
        <tbody>{weeks}</tbody>
      </table>
    );
  };
  
  return (
    <div>
      <h3>Calendar Component</h3>
      {renderCalendar()}
    </div>
  );
};

export default Calendercomponent;
