import { Routes, Route } from "react-router-dom";
import Dashboard from "./pages/Dashboard";

function router() {
  return (
    <Routes>
      <Route path="/" element />
      <Route path="/dashboard" element={<Dashboard />} />
    </Routes>
  );
}

export default router;
