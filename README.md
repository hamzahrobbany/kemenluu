<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Seal_of_the_Ministry_of_Foreign_Affairs_of_the_Republic_of_Indonesia.svg/1200px-Seal_of_the_Ministry_of_Foreign_Affairs_of_the_Republic_of_Indonesia.svg.png" width="400" alt="Kementerian Luar Negeri Logo"></a></p>

## Admin Kementerian Luar Negeri

## Installation Project

Project ini di buat mengggunakan React Js dengan Bundle plugins + Bootstrap Style, Cara instal seperti berikut:

- git Clone <git@gitlab.com>:hasyimas/admin-kemlu.git
- cd admin-kemlu.
- npm install && npm run dev (linux)
- npm install ; npm run dev (windows)
